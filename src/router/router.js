import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Home from '../components/Home/Home';
import SelectedComments from '../components/SelectedComments/SelectedComments';
import AddComment from '../components/AddComment/AddComment';

const router = ({ props }) => (
    <Router>
        <Route exact path="/">
            <Home props={ props } />
        </Route>
        <Route exact path="/selected-comments">
            <SelectedComments props={ props } />
        </Route>
        <Route exact path="/add-comment">
            <AddComment props={ props } />
        </Route>
    </Router>
)

export default router
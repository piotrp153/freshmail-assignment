export const GET_COMMENTS_SUCCESS = 'GET_COMMENTS_SUCCESS';
export const ADD_COMMENT = 'ADD_COMMENT';
export const ADD_SELECTED_COMMENT = 'ADD_SELECTED_COMMENT';
export const GET_ERROR = 'GET_ERROR';


export function getCommentsSuccess(comments) {
    return {
        type: GET_COMMENTS_SUCCESS,
        payload: comments
    }
}

export function addComment(comments) {
    return {
        type: ADD_COMMENT,
        payload: comments
    }
}

export function addSelectedComment(selectedComments) {
    return {
        type: ADD_SELECTED_COMMENT,
        payload: selectedComments
    }
}

export function getError(error) {
    return {
        type: GET_ERROR,
        payload: error
    }
}
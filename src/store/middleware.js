import { getCommentsSuccess, getError } from './actions';
import axios from 'axios';

export const getComments = () => async dispatch => {
    try {  
        let comments = [];

        const res = await axios.get(`https://jsonplaceholder.typicode.com/comments`);
        
        for (let i = 0; i < 20; i++) {
            comments = [ ...comments, res.data[i]];   
        }
        
        dispatch(getCommentsSuccess(comments));
    } catch (error) {
        console.log(error);
        dispatch(getError(error));
    }
};
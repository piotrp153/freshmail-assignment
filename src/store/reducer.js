import {GET_COMMENTS_SUCCESS, ADD_COMMENT, ADD_SELECTED_COMMENT, GET_ERROR } from './actions';

export const initialState = {
    comments: [],
    selectedComments: [],
}

export function commentsReducer(state = initialState, action) {
    switch(action.type) {
        case GET_COMMENTS_SUCCESS:
            return {
                ...state,
                comments: action.payload
            }
        case ADD_COMMENT:
            return {
                ...state,
                comments: action.payload
            }
        case ADD_SELECTED_COMMENT:
            return {
                ...state,
                selectedComments: action.payload
            }
        case GET_ERROR:
            return {
                ...state,
                error: action.payload,
            }
        default: 
            return state;
    }
}
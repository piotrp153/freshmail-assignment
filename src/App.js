import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { getComments } from './store/middleware';
import { addSelectedComment, addComment} from './store/actions';

import Router from './router/router';

const Container = styled.div`
        max-width: 1200px;
        margin: 20px auto;
        padding; 20px;
    `;

class App extends Component {
    componentDidMount() {
        const { getComments } = this.props;
        
        getComments();
    }
    render() {
        return (
            <Container>
                <Router props={ this.props } />
            </Container>
        )
    }
}

App.propTypes = {
    comments: PropTypes.array.isRequired,
    selectedComments: PropTypes.array.isRequired,
    getComments: PropTypes.func.isRequired,
    addSelectedComments: PropTypes.func.isRequired,
    addComment: PropTypes.func.isRequired,
};


const mapStateToProps = state => ({
    comments: state.comments.comments,
    selectedComments: state.comments.selectedComments,
})

const mapDispatchToProps = dispatch => ({
    getComments: () => dispatch(getComments()),
    addSelectedComments: comment => dispatch(addSelectedComment( comment )),
    addComment: comment => dispatch(addComment(comment)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)( App );

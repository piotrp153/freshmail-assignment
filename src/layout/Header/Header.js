import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { device } from '../../device/device';

const header = props => {
    const Header = styled.div`
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;


        @media ${device.tablet} {
            flex-direction: row;
        }
    `;

    return(
        <Header>
            { props.children }
        </Header>
    )
}

header.propTypes = {
    props: PropTypes.object,
};

export default header;
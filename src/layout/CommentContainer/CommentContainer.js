import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const commentContainer = props => {
    const CommentContainer = styled.div`
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
    `;
    
    return (
        <CommentContainer>
            { props.children }
        </CommentContainer>
    )
}

commentContainer.propTypes = {
    props: PropTypes.object,
};


export default commentContainer;
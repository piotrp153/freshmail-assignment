import React from 'react';
import PropTypes from 'prop-types';

import Header from '../../layout/Header/Header';
import HeaderLink from '../HeaderLink/HeaderLink';
import CommentContainer from '../../layout/CommentContainer/CommentContainer';
import Comment from '../Comment/Comment';

const home = ({ props }) => {
    return (
        <div>
            <Header>
                <HeaderLink link='/selected-comments' text='Selected comments' />
                <HeaderLink link='/add-comment' text='Add Comment' />
            </Header>
            <CommentContainer>
                {
                    props.comments.map((item, index) => (
                        <Comment 
                            key={ index } 
                            name={ item.name } 
                            email={ item.email } 
                            desc={ item.body } 
                            buttonEvent={ () => props.addSelectedComments([ ...props.selectedComments, item ]) } 
                            buttonText='Add to selected' 
                        />
                    ))
                }
            </CommentContainer>
        </div>
    )
}

home.propTypes = {
    props: PropTypes.object.isRequired,
};

export default home;
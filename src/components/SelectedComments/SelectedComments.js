import React from 'react';
import PropTypes from 'prop-types';

import Header from '../../layout/Header/Header';
import CommentContainer from '../../layout/CommentContainer/CommentContainer';
import Comment from '../Comment/Comment';
import HeaderLink from '../HeaderLink/HeaderLink';

const selectedComments = ({ props }) => {
    const removeElement = index => {
        const selectedComments = [ ...props.selectedComments ];
    
        selectedComments.splice(index, 1);
        props.addSelectedComments(selectedComments);
    }

    return (
        <div>
            <Header>
                <HeaderLink link='/' text='Back to Home' />
            </Header>
            <CommentContainer>
                {
                    props.selectedComments.map((item, index) => (
                        <Comment 
                            key={ index } 
                            name={ item.name } 
                            email={ item.email } 
                            desc={ item.body } 
                            buttonEvent={ () => removeElement(index) } 
                            buttonText='Remove from selected' 
                            buttonType='alert'
                        />
                    ))
                }
            </CommentContainer>
        </div>
    )
}

selectedComments.propTypes = {
    props: PropTypes.object.isRequired,
};

export default selectedComments;
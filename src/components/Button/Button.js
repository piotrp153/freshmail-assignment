import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

const button = ({ buttonType, text, buttonEvent }) => {
    const Button = styled.button`
        display: block;
        background: transparent;
        border-radius: 3px;
        border: 2px solid blue;
        color: blue;
        margin: 10px auto;
        padding: 5px 10px;
        cursor: pointer;
        outline: none;

        ${buttonType === 'alert' ? `
            border-color: red;
            color: red;
        `: ''}
    `;

    return (
        <Button onClick={ buttonEvent }>
            { text }
        </Button>
    )
}

button.propTypes = {
    buttonType: PropTypes.string,
    text: PropTypes.string.isRequired,
    buttonEvent: PropTypes.func.isRequired,
};

export default button;
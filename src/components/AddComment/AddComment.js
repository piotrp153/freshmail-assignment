import React from 'react';
import PropTypes from 'prop-types';

import Header from '../../layout/Header/Header';
import HeaderLink from '../HeaderLink/HeaderLink';
import Form from '../Form/Form';


const addComment = ({ props }) => {
    return (
        <div>
            <Header>
                <HeaderLink link='/' text='Back to Home' />
            </Header>
            <Form props={ props }/>
        </div>
    )
}

addComment.propTypes = {
    props: PropTypes.object.isRequired,
};

export default addComment;
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { device } from '../../device/device';

import Button from '../Button/Button';

const comment = ({ name, email, desc, buttonEvent, buttonText, buttonType }) => {
    const Comment = styled.div`
        width: 100%;
        padding: 10px;
        margin: 10px;
        box-sizing: border-box;
        box-shadow: 0 0 10px 2px rgba(0, 0, 0, .2);

        @media ${device.tablet} {
            width: calc(50% - 20px);
            margin: 10px 0;
        }

        @media ${device.laptop} {
            width: calc(33.3% - 20px);
        }
    `;

    const Quate = styled.h2`
        text-align: center;
    `;

    const Name = styled.p`
        font-weight: 500;
        margin-bottom: 10px;
        text-align: center;
    `;

    const Email = styled.p`
        font-size: 15px;
        margin: 0;
        text-align: center;
    `;

    return (
        <Comment>
            <Quate>
                { desc ? desc.substring(0,20): '' }
            </Quate>
            <Name>
                { name }
            </Name>
            <Email>
                { email }
            </Email>
            
            <Button buttonType={ buttonType } buttonEvent={ () => buttonEvent() } text={ buttonText } />
        </Comment>
    )
}

comment.propTypes = {
    name: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    desc: PropTypes.string.isRequired,
    buttonEvent: PropTypes.func.isRequired,
    buttonText: PropTypes.string.isRequired,
    buttonType: PropTypes.string,
};

export default comment;
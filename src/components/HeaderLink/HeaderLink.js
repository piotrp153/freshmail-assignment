import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const headerLink = ({ link, text }) => {
    const HeaderLink = styled(Link)`
        color: black;
        text-decoration: none;
        margin: 0 10px 20px;
        padding: 5px 10px;
        cursor: pointer;

        &:hover, &:focus {
            color: red;
        }
        
    `;
    
    return (
        <HeaderLink to={ link }>
            { text }
        </HeaderLink>
    )
}

headerLink.propTypes = {
    link: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
};

export default headerLink;
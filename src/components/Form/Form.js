import React from 'react';
import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';
import styled from 'styled-components';
import { device } from '../../device/device';

const Form = ({ props }) => {
    const Form = styled.form`
        display: flex;
        flex-direction: column;
        align-items: center;  
    `;

    const Input = styled.input`
        border: 1px solid black;
        padding: 5px;
        margin: 10px;
        width: 300px;

        @media ${device.tablet} {
            width: 500px;
        }
    `;

    const Submit = styled.input.attrs({ type: 'submit' })`
        display: block;
        background: transparent;
        border-radius: 3px;
        border: 2px solid blue;
        color: blue;
        margin: 10px auto;
        padding: 5px 10px;
        cursor: pointer;
        outline: none;
    `;

    const { register, handleSubmit } = useForm();
    const onSubmit = data => {
        let id = props.comments[props.comments.length - 1].id + 1;
        data = { ...data, id: id }

        props.addComment([ ...props.comments, data ]);
    };

    return (
        <Form onSubmit={ handleSubmit(onSubmit) }>
            <Input 
                name='name' 
                placeholder='Name' 
                ref={register({ required: true, pattern: /^[a-zA-Z]*$/, minLength: 3 })} 
            />

            <Input 
                name='email' 
                placeholder='Email' 
                ref={register({ required: true, pattern: /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/ })} 
            />
        
            <Input 
                name='body' 
                placeholder='Comment' 
                ref={register({ required: true, pattern: /^[a-zA-Z0-9]*$/, minLength: 2 })} 
            />

            <Submit type='submit' value='Add new comment' />
        </Form>
    )
}

Form.propTypes = {
    props: PropTypes.object.isRequired,
};


export default Form;